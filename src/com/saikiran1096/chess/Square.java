package com.saikiran1096.chess;

import com.saikiran1096.chess.piece.Piece;

/**
 * Created by sai on 7/3/14.
 */
public class Square {
    private int file; // file on the board
    private int rank; // rank on the board
    private Side side; // Color of the square
    private boolean selected; // true if this square is currently selected by a player

    private Piece occupyingPiece = null;

    Square(int file, int rank) {
        this.file = file;
        this.rank = rank;
        side = ((file + rank) % 2) == 1 ? Side.WHITE : Side.BLACK;
    }

    public Side getSide() {
        return side;
    }

    public int getFile() {
        return file;
    }

    public int getRank() {
        return rank;
    }

    public Piece getOccupyingPiece() {
        return occupyingPiece;
    }

    public void setOccupyingPiece(Piece occupyingPiece) {
        this.occupyingPiece = occupyingPiece;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    @Override
    public String toString() {
        return String.format("file: %d rank: %d", getFile(), getRank());
    }
}
