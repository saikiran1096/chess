package com.saikiran1096.chess.piece;

import com.saikiran1096.chess.Side;

/**
 * Created by sai on 7/22/14.
 */
public class Knight extends Piece {

    public Knight(Side side, int file, int rank) {
        super(side, file, rank);
    }

    @Override
    public boolean isValidMove(int file2, int rank2) {
        if (Math.abs(getFile() - file2) == 1 && Math.abs(getRank() - rank2) == 2) {
            return true;
        } else if (Math.abs(getFile() - file2) == 2 && Math.abs(getRank() - rank2) == 1) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public char getImageFileSuffix() {
        return 'n';
    }
}
