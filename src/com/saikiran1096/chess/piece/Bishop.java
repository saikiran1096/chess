package com.saikiran1096.chess.piece;

import com.saikiran1096.chess.Move;
import com.saikiran1096.chess.Side;

/**
 * Created by sai on 7/22/14.
 */
public class Bishop extends Piece {
    public Bishop(Side side, int file, int rank) {
        super(side, file, rank);
    }

    @Override
    public boolean isValidMove(int file2, int rank2) {
        System.out.printf("bishop: %d %d %d %d%n",getFile(),getRank(),file2,rank2);
        return Move.isDiagonalMovement(getFile(),getRank(), file2, rank2);
    }

    @Override
    public char getImageFileSuffix() {
        return 'b';
    }
}
