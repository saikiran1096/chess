package com.saikiran1096.chess.piece;

import com.saikiran1096.chess.Board;
import com.saikiran1096.chess.Side;

import java.io.Serializable;

/**
 * Created by sai on 7/3/14.
 */
public abstract class Piece implements Serializable {
    public static final String IMG_DIRECTORY = "res/sprites/";
    private Side side;
    private int file;
    private int rank;

    public Piece(Side side, int file, int rank) {
        if (!Board.isValidLocation(file,rank)) {
            throw new IllegalArgumentException("file and rank must be greater than 0 and less than 8");
        }
        this.side = side;
        this.file = file;
        this.rank = rank;
    }

    public abstract boolean isValidMove(int file2, int rank2);

    public int getFile() {
        return file;
    }

    public void setFile(int file) {
        this.file = file;
    }

    public int getRank() {
        return rank;
    }

    public void setRank(int rank) {
        this.rank = rank;
    }

    public Side getSide() {
        return side;
    }

    public String getImageFileName() {
        return IMG_DIRECTORY + getImageFilePrefix() + getImageFileSuffix() + ".png";
    }

    public char getImageFilePrefix() {
        return side.getImageFilePrefix();
    }

    public abstract char getImageFileSuffix();

    @Override
    public boolean equals(Object o) {
        if (o == null || !(o instanceof Piece)) return false;
        Piece piece = (Piece) o;
        return piece.getFile() == getFile()
                && piece.getRank() == getRank()
                && piece.getImageFileSuffix() == getImageFileSuffix()
                && piece.getSide() == getSide();
    }

    @Override
    public int hashCode() {
        int hash = (31 * 31 * 31 * file) + (31 * 31 * rank) + (31 * getImageFileSuffix());
        hash += getSide() == Side.WHITE ? 0 : 1;
        return hash;
    }

    public String toString() {
        char fileChar = (char) (65 + file);
        return String.format("%c%c%c%d", getImageFilePrefix(), getImageFileSuffix(), fileChar, rank + 1);
    }
}
