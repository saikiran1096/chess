package com.saikiran1096.chess.piece;

import com.saikiran1096.chess.Move;
import com.saikiran1096.chess.Side;

/**
 * Created by sai on 7/22/14.
 */
public class King extends Piece {

    private boolean hasMoved;

    public King(Side side, int file, int rank) {
        super(side, file, rank);
    }

    public boolean hasMoved() {
        return hasMoved;
    }

    @Override
    public boolean isValidMove(int file2, int rank2) {
        return (Move.isHorizontalMovement(this, file2, rank2) ||
                Move.isVerticalMovement(this, file2, rank2)
        );
    }


    @Override
    public char getImageFileSuffix() {
        return 'k';
    }
}
