package com.saikiran1096.chess.piece;

import com.saikiran1096.chess.Move;
import com.saikiran1096.chess.Side;

/**
 * Created by sai on 7/22/14.
 */
public class Pawn extends Piece {

    public Pawn(Side side, int file, int rank) {
        super(side, file, rank);
    }

    @Override
    public boolean isValidMove(int file2, int rank2) {
        if (isForwardMove(file2, rank2)) {
            switch (Math.abs(getRank() - rank2)) {
                case 1:
                    return true;
                case 2:
                    return hasNotMoved();
                default:
                    return false;
            }
        } else if (isForwardMove(file2, rank2) && Math.abs(getFile() - file2) == 1 && Math.abs(getRank() - rank2) == 1) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public char getImageFileSuffix() {
        return 'p';
    }

    private boolean isForwardMove(int file2, int rank2) {
        if (Move.isVerticalMovement(this, file2, rank2) || Move.isDiagonalMovement(this, file2, rank2)) {
            if (getSide() == Side.WHITE && rank2 > getRank()) {
                return true;
            } else if (getSide() == Side.BLACK && rank2 < getRank()) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    private boolean hasNotMoved() {
        if (getSide() == Side.WHITE) {
            return getRank() == 1;
        } else {
            return getRank() == 6;
        }
    }
}
