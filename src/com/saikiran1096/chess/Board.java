package com.saikiran1096.chess;

import com.saikiran1096.chess.piece.*;

import java.util.Scanner;

/**
 * Created by sai on 7/3/14.
 */
public class Board {
    public static final int BOARD_WIDTH = 8;
    public static final int BOARD_HEIGHT = 8;
    private Square[][] squares = new Square[BOARD_WIDTH][BOARD_HEIGHT];

   {
        for (int i = 0; i < BOARD_WIDTH; i++) {
            for (int j = 0; j < BOARD_HEIGHT; j++) {
                squares[i][j] = new Square(i, j);
            }
        }
    }

    public Board(String s) {
        Scanner scan = new Scanner(s);
        for (int rank = Board.BOARD_HEIGHT - 1; rank >= 0; rank--) {
            String[] line = scan.nextLine().split(" ");
            for (int file = 0; file < Board.BOARD_WIDTH; file++) {
                String p = line[file];
                if (p.equals("--")) {
                    continue;
                }
                char colorPrefix = p.charAt(0);
                char piecePrefix = p.charAt(1);
                Side side = colorPrefix == 'b' ? Side.BLACK : Side.WHITE;
                Piece piece;
                if (piecePrefix == 'b') {
                    piece = new Bishop(side, file, rank);
                } else if (piecePrefix == 'k') {
                    piece = new King(side, file, rank);
                } else if (piecePrefix == 'n') {
                    piece = new Knight(side, file, rank);
                } else if (piecePrefix == 'q') {
                    piece = new Queen(side, file, rank);
                } else if (piecePrefix == 'r') {
                    piece = new Rook(side, file, rank);
                } else {
                    piece = new Pawn(side, file, rank);
                }
                squares[file][rank].setOccupyingPiece(piece);

            }
        }
    }


    public static boolean isValidLocation(int file, int rank) {
        if (file < BOARD_WIDTH && rank < BOARD_HEIGHT && file >= 0 && rank >= 0) {
            return true;
        } else {
            return false;
        }
    }

    /*Tests if file is unoccupied on from rank1 to rank2 (exclusive).*/
    public boolean isFileFree(int file, int rank1, int rank2) {
        if (rank2 < rank1) {
            return isFileFree(file, rank2, rank1);
        }
        for (int rank = rank1 + 1; rank < rank2; rank++) {
            if (squares[file][rank].getOccupyingPiece() != null) {
                return false;
            }
        }
        return true;
    }

    /*Tests if rank is unoccupied on from file1 to file2 (exclusive).*/
    public boolean isRankFree(int rank, int file1, int file2) {
        if (file2 < file1) {
            return isRankFree(rank, file2, file1);
        }
        for (int file = file1 + 1; file < file2; file++) {
            if (squares[file][rank].getOccupyingPiece() != null) {
                return false;
            }
        }
        return true;
    }

    public boolean isDiagonalFree(int file1, int rank1, int file2, int rank2) {
        if (!Move.isDiagonalMovement(file1, rank1, file2, rank2)) {
            throw new IllegalArgumentException(String.format("(%d,%d) to (%d,%d) is not a diagonal movement", file1, rank1, file2, rank2));
        }

        if (file1 > file2) {
            return isDiagonalFree(file2, rank2, file1, rank1);
            /* so as to be able to check tiles from left to right on the board*/
        }

        if (rank1<rank2) {
            for (int i = 1; i < (file2-file1); i++) {
                /* return false if a tile on the diagonal is occupied*/
                if (squares[file1+i][rank1+i].getOccupyingPiece() != null) {
                    return false;
                }
            }
        } else {
            for (int i = 1; i < (file2-file1); i++) {
                 /* return false if a tile on the diagonal is occupied*/
                if (squares[file1+i][rank1-i].getOccupyingPiece() != null) {
                    return false;
                }
            }
        }
        return true;
    }

    public Square[][] getSquares() {
        return squares;
    }

    /* Removes the given piece from the board.*/
    public void removePiece(Piece piece) {
        if (!squares[piece.getFile()][piece.getRank()].getOccupyingPiece().equals(piece)) {
            throw new IllegalArgumentException(String.format("Piece %s is not on this board.", piece.toString()));
        }
        squares[piece.getFile()][piece.getRank()].setOccupyingPiece(null);
    }

    public void move(Piece piece, int destinationFile, int destinationRank) {
        if (piece == null) {
            throw new IllegalArgumentException("Argument piece was null");
        }
        Square originSquare = squares[piece.getFile()][piece.getRank()];
        Square destinationSquare = squares[destinationFile][destinationRank];
        Piece movingPiece = originSquare.getOccupyingPiece();

        if (!piece.equals(movingPiece)) {
            throw new IllegalArgumentException(String.format("Argument piece %s does not exist on this board.", piece.toString()));
        }

        if(piece instanceof Rook){
            ((Rook)piece).enableMoved();
        }

        removePiece(movingPiece);
        movingPiece.setFile(destinationFile);
        movingPiece.setRank(destinationRank);
        destinationSquare.setOccupyingPiece(movingPiece);
    }

    @Override
    public String toString() {
        StringBuilder string = new StringBuilder();
        for (int rank = BOARD_HEIGHT - 1; rank >= 0; rank--) {
            for (int file = 0; file < BOARD_WIDTH; file++) {
                Piece p = squares[file][rank].getOccupyingPiece();
                if (p != null) {
                    string.append(p.getImageFilePrefix());
                    string.append(p.getImageFileSuffix());
                } else {
                    string.append("--");
                }
            }
            string.append('\n');
        }
        return string.toString();
    }
}
