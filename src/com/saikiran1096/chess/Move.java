package com.saikiran1096.chess;

import com.saikiran1096.chess.piece.*;

import java.io.Serializable;
import java.lang.reflect.Constructor;
import java.util.List;
import java.util.Set;

/**
 * Created by sai on 8/25/14.
 */
public class Move implements Serializable {

    private Set<Piece> piecesRemoved;
    private List<Movement> movements;
    private char[] fileLetters = {'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h'};

    public Move(Set<Piece> piecesRemoved, List<Movement> movements) {
        this.movements = movements;
        this.piecesRemoved = piecesRemoved;
    }


    public static boolean isMovement(Piece p, int file2, int rank2) {
        return isMovement(p.getFile(), p.getRank(), file2, rank2);
    }

    public static boolean isMovement(int file1, int rank1, int file2, int rank2) {
        return !(file1 == file2 && rank1 == rank2);
    }

    public static boolean isHorizontalMovement(Piece p, int file2, int rank2) {
        return isHorizontalMovement(p.getFile(), p.getRank(), file2, rank2);
    }

    public static boolean isHorizontalMovement(int file1, int rank1, int file2, int rank2) {
        return file1 != file2 && rank1 == rank2;
    }

    public static boolean isVerticalMovement(Piece p, int file2, int rank2) {
        return isVerticalMovement(p.getFile(), p.getRank(), file2, rank2);
    }

    public static boolean isVerticalMovement(int file1, int rank1, int file2, int rank2) {
        return file1 == file2 && rank1 != rank2;
    }

    public static boolean isDiagonalMovement(Piece p, int file2, int rank2) {
        System.out.printf("bishop2 %d %d %d %d%n",p.getFile(),p.getRank(),file2,rank2);
        return isDiagonalMovement(p.getFile(), p.getRank(), file2, rank2);
    }

    public static boolean isDiagonalMovement(int file1, int rank1, int file2, int rank2) {
        System.out.printf("BISHOP3 " +
                "%d %d %d %d%n", file1, rank1, file2, rank2);
        System.out.println(Math.abs(file1 - file2) == Math.abs(rank1 - rank2));
        return isMovement(file1, rank1, file2, rank2) && Math.abs(file1 - file2) == Math.abs(rank1 - rank2);
    }

    public Set<Piece> getPiecesRemoved() {
        return piecesRemoved;
    }

    public List<Movement> getMovements() {
        return movements;
    }

    @Override
    public String toString() {
        StringBuilder string = new StringBuilder();

        if (!getMovements().isEmpty()) {
            Movement primaryMovement = getMovements().get(0);
            Piece primaryPiece = primaryMovement.getPiece();
            int destinationFile = primaryMovement.getDestinationFile();
            int destinationRank = primaryMovement.getDestinationRank();
            char pieceName = 0;

            if (primaryPiece instanceof King) {
                pieceName = 'K';
            } else if (primaryPiece instanceof Queen) {
                pieceName = 'Q';
            } else if (primaryPiece instanceof Bishop) {
                pieceName = 'B';
            } else if (primaryPiece instanceof Rook) {
                pieceName = 'R';
            } else if (primaryPiece instanceof Knight) {
                pieceName = 'N';
            }

            if (pieceName != 0) {
                string.append(pieceName);
            }

            string.append(fileLetters[primaryPiece.getFile()]);
            string.append(primaryPiece.getRank() + 1);

            boolean isCapture = false;

            for (Piece p : piecesRemoved) {
                if (p.getFile() == destinationFile && p.getRank() == destinationRank) {
                    isCapture = true;
                }
            }

            string.append(isCapture ? 'x' : '-');

            if (pieceName != 0) {
                string.append(pieceName);
            }

            string.append(fileLetters[destinationFile]);
            string.append(destinationRank + 1);

        }

        return string.toString();
    }

    public static class Movement implements Serializable {

        private Piece piece;
        private Class<? extends Piece> pieceType;
        private Side pieceSide;
        private int originFile;
        private int originRank;
        private int destinationFile;
        private int destinationRank;

        Movement(Piece piece, int destinationFile, int destinationRank) {
            if (!Board.isValidLocation(destinationFile, destinationRank)) {
                String errorMessage = String.format("%d %d is not a valid location on the board.");
                throw new IllegalArgumentException(errorMessage);
            }
            pieceType = piece.getClass();
            pieceSide = piece.getSide();
            originFile = piece.getFile();
            originRank = piece.getRank();
            this.destinationFile = destinationFile;
            this.destinationRank = destinationRank;
        }

        public Piece getPiece() {

            if (piece == null) {
                try {
                    //this is gross
                    Constructor<? extends Piece> constructor = pieceType.getConstructor(Side.class, Integer.TYPE, Integer.TYPE);
                    piece = constructor.newInstance(pieceSide, originFile, originRank);
                } catch (Exception e) {
                    //TODO log
                    e.printStackTrace();
                }
            }

            return piece;
        }

        public int getDestinationRank() {
            return destinationRank;
        }

        public int getDestinationFile() {
            return destinationFile;
        }

        @Override
        public String toString() {
            return String.format("%s %d %d", getPiece().toString(), destinationFile, destinationRank);
        }


    }
}
