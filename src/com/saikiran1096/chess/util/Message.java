package com.saikiran1096.chess.util;

import java.io.Serializable;
import java.util.Map;
import java.util.TreeMap;

/**
 * Created by sai on 7/17/14.
 */
public class Message implements Serializable {

    Map<String, Object> items = new TreeMap<>();
    private String type;

    public Message(String type) {
        this.type = type;
    }

    public String getType() {
        return type;
    }

    public Object getItem(String key) {
        return items.get(key);
    }

    public void setItem(String key, Object value) {
        items.put(key, value);
    }

    @Override
    public String toString() {
        return String.format("%s: %s", type, items.toString());
    }

}
