package com.saikiran1096.chess.util;

import javax.swing.*;
import java.time.LocalDateTime;

/**
 * Created by sai on 7/19/14.
 */
public class ServerTextArea extends JTextArea {
    public ServerTextArea() {
        super(20, 75);
    }

    public void println(String s) {
        Runnable r = () -> {
            String time = LocalDateTime.now().toString();
            append(String.format("[%s]: %s%n", time, s));
        };
        SwingUtilities.invokeLater(r);
    }
}