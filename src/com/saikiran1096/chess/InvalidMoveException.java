package com.saikiran1096.chess;

/**
 * Created by sai on 11/1/14.
 */
public class InvalidMoveException extends Exception {
    InvalidMoveException() {
        super();
    }
    

    InvalidMoveException(String message) {
        super(message);
    }
}
