package com.saikiran1096.chess.net;


import com.saikiran1096.chess.util.Message;
import com.saikiran1096.chess.util.ServerTextArea;

import javax.swing.*;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.net.ServerSocket;
import java.util.ArrayList;
import java.util.List;


public class ChessServer implements Runnable {
    public static final int PORT_NUMBER = 6931;
    private List<Connection> clients = new ArrayList<>();
    private ServerTextArea serverTextArea;
    private ChessServerProtocol chessServerProtocol;
    private ServerSocket serverSocket;

    ChessServer(ServerSocket s) {
        serverSocket = s;
        chessServerProtocol = new ChessServerProtocol(this);
    }

    public static void main(String[] args) throws InterruptedException {
        JFrame frame = new JFrame("ChessServer");
        ServerTextArea textArea = new ServerTextArea();
        JScrollPane scrollPane = new JScrollPane(textArea);
        frame.add(scrollPane);

        frame.pack();
        frame.setLocationRelativeTo(null);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setVisible(true);

        ChessServer chessServer;
        try {
            ServerSocket serverSocket = new ServerSocket(ChessServer.PORT_NUMBER);
            chessServer = new ChessServer(serverSocket);
            chessServer.setTextArea(textArea);
            textArea.println(String.format("Opened server socket on port %d.", ChessServer.PORT_NUMBER));
            new Thread(chessServer).start();
        } catch (IOException e) {
            e.printStackTrace();
            textArea.println(String.format("Could not create server socket on port %d.", ChessServer.PORT_NUMBER));
            System.exit(1);
        }
    }

    public void run() {
        try {
            while (!serverSocket.isClosed()) {
                Connection client = new Connection(serverSocket.accept());
                clients.add(client);
                Thread listeningThread = new Thread(() -> {
                    ObjectInputStream inputStream = client.getInput();
                    Message message;
                    try {
                        while ((message = (Message) inputStream.readObject()) != null) {
                            chessServerProtocol.processMessage(message, client);
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    } catch (ClassNotFoundException e) {
                        e.printStackTrace();
                    }
                });
                listeningThread.start();
            }
        } catch (IOException e) {
            e.printStackTrace();

        }
    }

    public List<Connection> getClients() {
        return clients;
    }

    public void setTextArea(ServerTextArea serverTextArea) {
        this.serverTextArea = serverTextArea;
    }

    public ServerTextArea getServerTextArea() {
        return serverTextArea;
    }

}