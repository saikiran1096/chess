package com.saikiran1096.chess.net;

import com.saikiran1096.chess.*;
import com.saikiran1096.chess.gfx.BoardDisplay;
import com.saikiran1096.chess.gfx.TileDisplay;
import com.saikiran1096.chess.util.Message;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.net.Socket;
import java.util.Stack;

public class ChessClient implements MouseListener {
    Stack<Square> squareStack = new Stack<>();
    private Position position;
    private JFrame frame;
    private Side side;
    private BoardDisplay boardDisplay;
    private ChessClientProtocol chessClientProtocol;
    private Connection connection;


    ChessClient() {
        frame = new JFrame("Chess");
        JMenuBar menuBar = new JMenuBar();
        JMenu fileMenu = new JMenu("File");
        JMenu serverMenu = new JMenu("Server");
        JMenu viewMenu = new JMenu("View");
        setPosition();
        chessClientProtocol = new ChessClientProtocol(this);

        JMenuItem serverItem = new JMenuItem("Connect to Server");
        serverMenu.add(serverItem);

        serverItem.addActionListener((ActionEvent e) -> {
            JFrame connectFrame = new JFrame("Connect to Server");
            connectFrame.getContentPane().setLayout(new GridLayout(1, 2));
            connectFrame.add(new JLabel("Host server IP:"));
            JTextField textField = new JTextField(15);
            textField.addActionListener((ActionEvent ee) -> {
                try {
                    connect(textField.getText(), 6931);
                    connectFrame.setVisible(false);
                    frame.setTitle("Chess - Connected");
                } catch (IOException e1) {
                    e1.printStackTrace();
                }
            });
            connectFrame.add(textField);
            connectFrame.pack();
            connectFrame.setLocationRelativeTo(null);
            connectFrame.setVisible(true);
        });

        menuBar.add(fileMenu);
        menuBar.add(serverMenu);
        menuBar.add(viewMenu);
        frame.setJMenuBar(menuBar);


        frame.pack();
        frame.setLocationRelativeTo(null);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setVisible(true);
    }

    public static void main(String[] args) throws IOException {
        ChessClient client = new ChessClient();
        if(args.length == 1){
            client.connect(args[0],6931);
        }
    }

    public void connect(String host, int port) throws IOException {
        this.connection = new Connection(new Socket(host, port));
        Thread listeningThread = new Thread(() -> {
            ObjectInputStream inputStream = connection.getInput();
            try {
                connection.sendMessage(new Message("connect"));
            } catch (IOException e) {
                e.printStackTrace();
            }
            Message message;
            try {
                while ((message = (Message) inputStream.readObject()) != null) {
                    chessClientProtocol.processMessage(message);
                }
            } catch (IOException e) {
                e.printStackTrace();
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
        });
        listeningThread.start();
    }

    public Position getPosition() {
        return position;
    }

    public void setPosition(Position position) {
        this.position = position;
        if (boardDisplay != null)
            frame.remove(boardDisplay);
        boardDisplay = new BoardDisplay(this.position.getBoard());
        frame.add(boardDisplay);
        frame.pack();
        for (TileDisplay[] i : boardDisplay.getTileDisplays()) {
            for (TileDisplay j : i) {
                j.addMouseListener(this);
            }
        }
    }

    public BoardDisplay getBoardDisplay() {
        return boardDisplay;
    }


    public void moveMessage(Move move) throws IOException {
        Message m = new Message("move");
        m.setItem("move", move);
        connection.sendMessage(m);
    }

    @Override
    public void mouseClicked(MouseEvent event) {
        Square clickedSquare = ((TileDisplay) (event.getComponent())).getSquare();
        if (squareStack.size() == 0) {
            if (clickedSquare.getOccupyingPiece() != null && clickedSquare.getOccupyingPiece().getSide() == side) {
                squareStack.push(clickedSquare);
                clickedSquare.setSelected(true);
                boardDisplay.repaint();
            }
        } else {
            Square originSquare = squareStack.pop();
            originSquare.setSelected(false);
            if (position.getSideToMove() == side) {
                Square destinationSquare = clickedSquare;
                int originFile = originSquare.getFile();
                int originRank = originSquare.getRank();
                int destinationFile = destinationSquare.getFile();
                int destinationRank = destinationSquare.getRank();

                try {
                    Move move = position.createMove(originFile, originRank, destinationFile, destinationRank);
                    moveMessage(move);
                } catch (InvalidMoveException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            boardDisplay.repaint();
        }
    }

    @Override
    public void mousePressed(MouseEvent e) {

    }

    @Override
    public void mouseReleased(MouseEvent e) {

    }

    @Override
    public void mouseEntered(MouseEvent e) {

    }

    @Override
    public void mouseExited(MouseEvent e) {

    }

    public void setSide(Side side) {
        this.side = side;
        frame.setTitle("Chess " + side.toString());
    }
}