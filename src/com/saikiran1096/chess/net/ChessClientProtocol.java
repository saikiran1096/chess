package com.saikiran1096.chess.net;

import com.saikiran1096.chess.Position;
import com.saikiran1096.chess.Move;
import com.saikiran1096.chess.Side;
import com.saikiran1096.chess.util.Message;

/**
 * Created by sai on 8/2/14.
 */
public class ChessClientProtocol {
    private ChessClient chessClient;

    ChessClientProtocol(ChessClient chessClient) {
        this.chessClient = chessClient;
    }

    public void processMessage(Message m) {
        switch (m.getType()) {
            case "newGame":
                chessClient.setPosition(new Position());
                chessClient.setSide((Side) m.getItem("side"));
                break;
            case "move":
                Move move = (Move) m.getItem("move");
                chessClient.getPosition().processMove(move);
                break;
        }
    }
}
