package com.saikiran1096.chess.net;

import com.saikiran1096.chess.Side;
import com.saikiran1096.chess.util.Message;
import com.saikiran1096.chess.util.ServerTextArea;

import java.io.IOException;

/**
 * Created by sai on 7/19/14.
 */
public class ChessServerProtocol {
    private ChessServer chessServer;

    ChessServerProtocol(ChessServer cs) {
        chessServer = cs;
    }

    public void processMessage(Message m, Connection client) {
        if (chessServer.getServerTextArea() != null) {
            ServerTextArea serverTextArea = chessServer.getServerTextArea();
            serverTextArea.println(String.format("Processing message - %s", m.toString()));
        }
        switch (m.getType()) {
            case "move":
            case "text":
                relayMessage(m);
                break;
            case "connect":
                try {
                    client.sendMessage(new Message("nice i got it!!!!"));
                } catch (IOException e) {
                    e.printStackTrace();
                }
                if (chessServer.getClients().size() == 2) {
                    newGame();
                }
                break;
        }
    }

    public void relayMessage(Message m) {
        if (chessServer.getServerTextArea() != null) {
            ServerTextArea serverTextArea = chessServer.getServerTextArea();
            serverTextArea.println(String.format("Relaying message - %s", m.toString()));
        }
        for (Connection con : chessServer.getClients()) {
            try {
                con.sendMessage(m);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public void newGame() {
        if (chessServer.getServerTextArea() != null) {
            ServerTextArea serverTextArea = chessServer.getServerTextArea();
            serverTextArea.println("Starting a new game");
        }
        Message white = new Message("newGame");
        white.setItem("side", Side.WHITE);
        Message black = new Message("newGame");
        black.setItem("side", Side.BLACK);
        try {
            chessServer.getClients().get(0).sendMessage(white);
            chessServer.getClients().get(1).sendMessage(black);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
