package com.saikiran1096.chess.net;

import com.saikiran1096.chess.Side;
import com.saikiran1096.chess.util.Message;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;

/**
 * Created by sai on 8/4/14.
 */
public class Connection {
    private ObjectOutputStream output;
    private ObjectInputStream input;
    private Side side;

    Connection(Socket socket) throws IOException {
        output = new ObjectOutputStream(socket.getOutputStream());
        input = new ObjectInputStream(socket.getInputStream());
    }

    public void sendMessage(Message m) throws IOException {
        output.writeObject(m);
        output.flush();
    }

    public Side getSide() {
        return side;
    }

    public void setSide(Side side) {
        this.side = side;
    }

    public ObjectInputStream getInput() {
        return input;
    }
}
