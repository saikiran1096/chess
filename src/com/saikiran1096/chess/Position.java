package com.saikiran1096.chess;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * Created by sai on 3/20/15.
 */
public class Position {
    private int halfmoveClock = 0;
    private boolean[] castleRights = new boolean[4];
    private List<Square> enPassantCandidates = new ArrayList<>();
    private Board board;

    public Side getSideToMove() {
        return sideToMove;
    }

    public Board getBoard() {
        return board;
    }

    private Side sideToMove = Side.WHITE;
    private static final String DEFAULT_POSITION = "res/games/default.game";

    public Position(){
        try {
            File f = new File(DEFAULT_POSITION);
            Scanner scan = new Scanner(f);
            sideToMove = Side.valueOf(scan.nextLine());
            StringBuilder boardString = new StringBuilder();
            while (scan.hasNextLine()) {
                boardString.append(scan.nextLine());
                boardString.append('\n');
            }
            board = new Board(boardString.toString());
        } catch (FileNotFoundException e) {
            InputStream f = getClass().getResourceAsStream("/" + DEFAULT_POSITION);

            Scanner scan = new Scanner(f);
            sideToMove = Side.valueOf(scan.nextLine());
            StringBuilder boardString = new StringBuilder();
            while (scan.hasNextLine()) {
                boardString.append(scan.nextLine());
                boardString.append('\n');
            }
            board = new Board(boardString.toString());
        }
    }


    public void processMove(Move m){
        if(sideToMove == Side.WHITE){
            sideToMove = Side.BLACK;
        }else{
            sideToMove = Side.WHITE;
        }
    }

}
