package com.saikiran1096.chess;

import java.awt.*;
import java.io.Serializable;

/**
 * Created by sai on 8/1/14.
 */
public enum Side implements Serializable {
    BLACK(new Color(180, 135, 100), new Color(220, 195, 75), 'b'), WHITE(new Color(240, 220, 180), new Color(240, 230, 100), 'w');

    private final Color tileColor;
    private final Color hightlightColor;
    private final char imageFilePrefix;

    Side(Color tileColor, Color highlightColor, char imageFilePrefix) {
        this.tileColor = tileColor;
        this.hightlightColor = highlightColor;
        this.imageFilePrefix = imageFilePrefix;
    }

    public Color getTileColor() {
        return tileColor;
    }

    public Color getHightlightColor() {
        return hightlightColor;
    }

    public char getImageFilePrefix() {
        return imageFilePrefix;
    }

}
