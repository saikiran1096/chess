package com.saikiran1096.chess.gfx;

import com.saikiran1096.chess.Square;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;

/**
 * Created by sai on 7/4/14.
 */
public class TileDisplay extends JPanel {

    private Square square;

    TileDisplay(Square square) {
        this.square = square;
        this.setBackground(square.getSide().getTileColor());
        this.setPreferredSize(new Dimension(80, 80));
    }

    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        if (square.isSelected()) {
            setBackground(square.getSide().getHightlightColor());
        } else {
            setBackground(square.getSide().getTileColor());
        }
        if (square.getOccupyingPiece() != null) {
            File f = new File(square.getOccupyingPiece().getImageFileName());
            BufferedImage bufferedImage;
            try {
                bufferedImage = ImageIO.read(f);
                g.drawImage(bufferedImage, 0, 0, null);
                int shorterSideLength = Math.min(getHeight(), getWidth());
                Image scaledImage = bufferedImage.getScaledInstance(shorterSideLength, shorterSideLength, Image.SCALE_FAST);
                g.drawImage(scaledImage, (getWidth() - shorterSideLength) / 2, (getHeight() - shorterSideLength) / 2, null);
            } catch (IOException e) {
                try (InputStream fileStream = getClass().getResourceAsStream("/" + square.getOccupyingPiece().getImageFileName())) {
                    bufferedImage = ImageIO.read(fileStream);
                    int shorterSideLength = Math.min(getHeight(), getWidth());
                    Image scaledImage = bufferedImage.getScaledInstance(shorterSideLength, shorterSideLength, Image.SCALE_FAST);
                    g.drawImage(scaledImage, (getWidth() - shorterSideLength) / 2, (getHeight() - shorterSideLength) / 2, null);
                } catch (IOException e1) {
                    e1.printStackTrace();
                }
            }
        }
    }

    public Square getSquare() {
        return square;
    }


}


