package com.saikiran1096.chess.gfx;

import com.saikiran1096.chess.Board;
import com.saikiran1096.chess.Square;

import javax.swing.*;
import java.awt.*;

/**
 * Created by sai on 7/4/14.
 */
public class BoardDisplay extends JPanel {
    Board board;
    TileDisplay[][] tileDisplays = new TileDisplay[Board.BOARD_HEIGHT][Board.BOARD_WIDTH];
    Square[][] squares;

    public BoardDisplay(Board board) {
        super(new GridLayout(8, 8));
        this.board = board;
        squares = board.getSquares();
        for (int file = 0; file < Board.BOARD_WIDTH; file++) {
            for (int rank = 0; rank < Board.BOARD_HEIGHT; rank++) {
                tileDisplays[file][rank] = new TileDisplay(squares[rank][file]);
            }
        }

        for (int file = Board.BOARD_WIDTH - 1; file >= 0; file--) {
            for (int rank = 0; rank < Board.BOARD_HEIGHT; rank++) {
                add(tileDisplays[file][rank]);
            }
        }

    }

    public TileDisplay[][] getTileDisplays() {
        return tileDisplays;
    }

}
